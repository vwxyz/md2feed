#modules
async      = require "async"
fs         = require "fs"
request    = require "request"
RSS        = require "rss"
markdown   = require "node-markdown"
md         = markdown.Markdown
mdPath     = "#{process.cwd()}/md/dl.md"
cheerio    = require "cheerio"

# load http://webplatformdaily.org/data/latest.md
module.exports = ->
	console.log "<Q> dl.md is downloaded? <A> #{fs.existsSync mdPath} "
	tasks = []
	tasks.push (cb)->
		if !fs.existsSync mdPath
			console.log "not downloaded yet .md file.."
			request(
				"http://webplatformdaily.org/data/latest.md"
				(err,response,body)->
					fs.writeFile "#{process.cwd()}/md/dl.md",body,(err)->
						console.log "dl.md created...."
						cb null
			)
		else
			cb null
	tasks.push (cb)->
		console.log "next"
		rssGen()
		cb null
	async.series tasks,(err)-> console.log "async.series() end."

	cronJob     = require("cron").CronJob
	cronTimeDL  = "00  0-23/6  *  *  *" #6時間ごとに
	cronTimeGen = "05  0-23/6  *  *  *"
	# cronTimeDL  = "05 * * * * *"
	# cronTimeGen = "00 * * * * *"

	jobDL = new cronJob(
		cronTime: cronTimeDL
		onTick: ()->
			console.log "cronTimeDL() start...."
			request(
				"http://webplatformdaily.org/data/latest.md"
			).pipe(
				fs.createWriteStream "#{process.cwd()}/md/dl.md"
			)		
		onComplete:()-> console.log "\u001b[33m" + "job completed:"+(new Date()) + "\u001b[39m"
		start: true
		# timeZone: "Japan/Tokyo"
	)
	jobGen = new cronJob(
		cronTime: cronTimeGen
		onTick: rssGen
		onComplete:()-> console.log "\u001b[33m" + "job completed:"+(new Date()) + "\u001b[39m"
		start: true
		# timeZone: "Japan/Tokyo"
	)
	#run
	jobDL.start()
	jobGen.start()
# create rss
rssGen = ()->
	console.log "rssGen():called..."
	if fs.existsSync mdPath
		fs.readFile mdPath,"utf-8",(err,data)->
			html = md data
			# cheerio
			$ = cheerio.load html
			$("a").each (i,item)->
				if item.children[0].data.charAt(0) is "@"
					item.children[0].parent.attribs.href = "http://twitter.com/#{item.children[0].data.substr 1,}"
			html = $.html()
			console.log html
			arr  =[]
			# console.log typeof html
			arr  = html.split "</ul>\n\n"
			_html = []
			for i,j of arr[1..]
				j = "#{j}</ul>"
				j = j.substr 4
				j = j.split "</h2>\n\n"
				_html.push j
			# for m in _html[..5]
			# 	console.log "---------"
			# 	console.log m
			rss = new RSS(
				title: "Open Web Platform Daily Digest"
				description: "un-official feed of webplatformdaily.org"
				link: "http://webplatformdaily.org/"
				site_url: "http://webplatformdaily.org/"
				author: "Šime Vidas"
			)
			for i in _html
				# console.log i[1]
				rss.item
					title: i[0]
					description: i[1]
					url: "http://webplatformdaily.org/"
					guid: i[0].replace(/(\s|\,)+/gi,"").toLowerCase()
					# date: i[0]
			xml = rss.xml()
			fs.writeFile "#{__dirname}/public/rss/feed.rss", xml,(err)->
				console.log "rss:saved..."		
