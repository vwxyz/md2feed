# module
express = require("express")
routes = require("./routes")
feed   = require "./routes/feed"
user = require("./routes/user")
http = require("http")
path = require("path")

app = express()
app.configure ->
  app.set "port", process.env.VCAP_APP_PORT or 3000
  app.set "views", __dirname + "/views"
  app.set "view engine", "jade"
  app.use express.favicon()
  app.use express.logger("dev")
  app.use express.bodyParser()
  app.use express.methodOverride()
  app.use app.router
  app.use express.static(path.join(__dirname, "public"))
app.configure "development", ->
  app.use express.errorHandler()


#routing
app.get "/", (req,res)->
  res.redirect "/feed"
  # routes.index
app.get "/feed", feed.file
app.get "/md", feed.md
app.get "/users", (req,res)->
  res.redirect "/feed"
  #user.list


#server listens
http.createServer(app).listen app.get("port"), ->
  console.log "Express server listening on port " + app.get("port")



#cron
parse = require "./parse"
parse()

