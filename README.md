# md2feed

- download .md file from http://webplatformdaily.org/data/latest.md
- parse .md file
- build .rss file
- routes "/webplatformdaily" to .rss file
- deploy at heroku
- http://webplatformdaily-org.ap01.aws.af.cm/feed
- `gem install af`
